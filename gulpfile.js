var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var cleanCSS = require('gulp-clean-css');

gulp.task('styles', function() {
    return gulp.src(['scss/*.scss'])
        .pipe(sass({
            includePaths: [
            './node_modules/'
            ]
        }))
        .pipe(concat('styles.min.css'))
        .pipe(cleanCSS())
        .pipe(gulp.dest('./'));
});

gulp.task('minify-css', function() {
    return gulp.src('styles.css')
        .pipe(cleanCSS({compatibility: 'ie9'}))
        .pipe(gulp.dest('./'));
});

gulp.task('default', ['styles','minify-css']);