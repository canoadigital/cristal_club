# Requirements
- Node
- NPM

# Installation

## Clone the repository
Clone the repository

```
    git clone https://bitbucket.org/canoadigital/cristal_club.git
```

## Install the dependencies

From the app root folder, run:

```
    npm install
```
